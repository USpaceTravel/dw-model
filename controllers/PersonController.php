<?php

require 'models/Person.php';

class PersonController
{

    private $model;
    private $jsonRes;


    public function __construct()
    {
        $this->model = new Person("","","","","");
    }

    public function index()
    {
        $query = $this->model->getAll();
        $x = $this->model->readArchive(".\database\dw-model.txt","|");
        $y = $this->model->readArchive(".\database\dw-model.csv",",");
        require 'views/home.php';
        
        require 'views/footer.php';
    }

    public function add()
    {
        require 'views/home.php';
    }

    public function save()
    {
        if (isset($_POST)) {

            if ($this->model->newPerson($_REQUEST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo json_encode(['success' => 'vacio']);
        }
    }

    public function edit()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = $this->model->getById($id);

            require 'views/footer.php';
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            if ($this->model->editPerson($_POST)) {

                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo json_encode(['success' => 'vacio']);
        }
    }

    public function delete()
    {
        if ($this->model->deletePerson($_POST)) {

            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
    }

    public function getidinfo()
    {

        $query = $this->model->getById($_REQUEST['id']);

        if ($query) {

            echo json_encode(['mess' => 'Se consultó', 'result' => true, 'row' => $query]);
        } else {
            echo json_encode(['mess' => 'No se consultó', 'result' => false]);
        }
    }
}
