-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-04-2022 a las 05:31:12
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dw-model`
--
CREATE DATABASE IF NOT EXISTS `dw-model` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `dw-model`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` char(1) NOT NULL,
  `edad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `person`
--

insert into person (id, name, last_name, gender, edad) values (1, 'Shurlocke', 'Eatock', 'M', 1);
insert into person (id, name, last_name, gender, edad) values (2, 'Marthena', 'Winny', 'F', 2);
insert into person (id, name, last_name, gender, edad) values (3, 'John', 'Oherlihy', 'M', 3);
insert into person (id, name, last_name, gender, edad) values (4, 'Fan', 'Purves', 'F', 4);
insert into person (id, name, last_name, gender, edad) values (5, 'Jolee', 'Sanzio', 'F', 5);
insert into person (id, name, last_name, gender, edad) values (6, 'Beryle', 'Duly', 'F', 6);
insert into person (id, name, last_name, gender, edad) values (7, 'Annabella', 'Gerlts', 'F', 7);
insert into person (id, name, last_name, gender, edad) values (8, 'Penelope', 'Ezzle', 'F', 8);
insert into person (id, name, last_name, gender, edad) values (9, 'Osbert', 'Dewdney', 'M', 9);
insert into person (id, name, last_name, gender, edad) values (10, 'Jilli', 'Jozsika', 'F', 10);
insert into person (id, name, last_name, gender, edad) values (11, 'Blake', 'Lumm', 'M', 11);
insert into person (id, name, last_name, gender, edad) values (12, 'Ruddie', 'Trimmill', 'M', 12);
insert into person (id, name, last_name, gender, edad) values (13, 'Catarina', 'Dumphry', 'F', 13);
insert into person (id, name, last_name, gender, edad) values (14, 'John', 'Corradeschi', 'M', 14);
insert into person (id, name, last_name, gender, edad) values (15, 'Geoffry', 'Padwick', 'M', 15);
insert into person (id, name, last_name, gender, edad) values (16, 'Richy', 'Dodson', 'M', 16);
insert into person (id, name, last_name, gender, edad) values (17, 'Gunilla', 'Eickhoff', 'F', 17);
insert into person (id, name, last_name, gender, edad) values (18, 'Lina', 'Pyott', 'F', 18);
insert into person (id, name, last_name, gender, edad) values (19, 'Rodolph', 'Eich', 'M', 19);
insert into person (id, name, last_name, gender, edad) values (20, 'Brent', 'Arthey', 'M', 20);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
