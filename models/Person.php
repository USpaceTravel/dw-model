<?php


class Person
{

    public $id;
    public $name;
    public $last_name;
    public $gender;
    public $edad;

    private $pdo;

    public function __construct($id, $name, $last_name, $gender, $edad)
    {
        try {

            $this->pdo = new Database;
            $this->id = $id;
            $this->name = $name;
            $this->last_name = $last_name;
            $this->gender = $gender;
            $this->edad = $edad;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM person ";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } # fin metodo getAll

    public function newPerson($data)
    {
        try {
            if ($this->pdo->insert('person', $data)) {

                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newperson

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM person WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editPerson($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];

            if ($this->pdo->update('person', $data, $srtWhere)) {

                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deletePerson($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];

            if ($this->pdo->delete('person', $srtWhere)) {

                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
    public function readArchive($name, $separator){

        $archivo = fopen($name, "r");

        $people = array();

        while (!feof($archivo)){
            
            $linea = fgets($archivo);

            $data = explode( $separator, $linea);

            $datos = new Person($data[0], $data[1], $data[2], $data[3], $data[4]);
            array_push($people, $datos);

            
        }

        fclose($archivo);

        return $people;

    }

}
