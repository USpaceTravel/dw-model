<?php

function edad($edad)
{
  if ($edad <= 8) {
    return "Niño";
  } elseif ($edad <= 16 && $edad > 8) {
    return "Adolecente";
  } elseif ($edad <= 36 && $edad > 16) {
    return "Casi Cucho";
  } elseif ($edad > 36) {
    return "Cucho";
  }
}

function gender($gender)
{

  if ($gender == 'M') {

    return "Hombre";
  } else {
    return "Mujer";
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap" rel="stylesheet">

  <title>Inicio</title>

  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link href="/assets/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/style.css">

</head>

<header>
  <nav class="navbar navbar-light bg-light p-5">
    <div class="container-fluid">
      <span class="navbar-brand">Personas</span>
    </div>
  </nav>
</header>
<main class="container">
  <section class="col-md-12 py-5">
    <table class="table table-borderless">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre(s)</th>
          <th>Apellido(s)</th>
          <th>Género</th>
          <th>Edad</th>
          <th>Tipo</th>
        </tr>
      </thead>

      <tbody></tbody>
      <?php
      if (isset($query)) {
        foreach ($query as $queries) : ?>
          <tr>
            <td><?php echo $queries->id; ?></td>
            <td><?php echo ucwords($queries->name); ?></td>
            <td><?php echo ucwords($queries->last_name); ?></td>
            <td><?php echo gender($queries->gender); ?></td>
            <td><?php echo $queries->edad; ?></td>
            <td><?php echo edad($queries->edad); ?></td>
          </tr>
      <?php endforeach;
      }
      ?>
      <?php

      if (isset($x)) {
        foreach ($x as $xs) : ?>
          <tr>
            <td><?php echo $xs->id; ?></td>
            <td><?php echo ucwords($xs->name); ?></td>
            <td><?php echo ucwords($xs->last_name); ?></td>

            <td><?php echo $xs->gender; ?></td>
            <td><?php echo $xs->edad; ?></td>
            <td><?php echo edad($xs->edad); ?></td>
          </tr>
      <?php endforeach;
      }
      ?>

      <?php

      if (isset($y)) {
        foreach ($y as $ys) : ?>
          <tr>

            <td><?php echo $ys->id; ?></td>
            <td><?php echo ucwords($ys->name); ?></td>
            <td><?php echo ucwords($ys->last_name); ?></td>

            <td><?php echo $ys->gender; ?></td>
            <td><?php echo $ys->edad; ?></td>
            <td><?php echo edad($ys->edad); ?></td>
          </tr>
      <?php endforeach;
      }
      ?>
      </tbody>
    </table>
  </section>
</main>